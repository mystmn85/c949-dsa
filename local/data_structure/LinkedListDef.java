package local.data_structure;

import local.impl.CommonImpl;
import local.impl.LinkedListImpl;

import java.util.LinkedList;

public class LinkedListDef implements LinkedListImpl, CommonImpl {

    private final LinkedList<Integer> linkedList;

    public LinkedListDef(LinkedList<Integer> linkedList) {
        this.linkedList = linkedList;
    }

    @Override
    public void printContainer() {
        title(getClass().getSimpleName());
        System.out.println("**Symbol");;
        System.out.println("**Terms:  nodes that reference the next node");
        System.out.println("**Technique: getFirst,getLast,removeFirst,removeLast");
        sortedType(true, true);
        worstCase("O(n^2)");

        getTrainLinkedList();
    }

    public void getTrainLinkedList() {
        // LinkedList w/ containers
        System.out.println("\n **Process");
        this.addFirst(6);
        this.addFirst(8);
        this.removeFirst();
        this.addFirst(3);
        this.addFirst(4);
        this.addFirst(4);

        this.removeLast();
        this.getFirst();
        this.getLast();
    }

    @Override
    public void addFirst(Integer count) {
        System.out.println("add pos#0 container: "+ count.toString());
        linkedList.addFirst(count);
    }

    @Override
    public void removeFirst() {
        System.out.println("removeFirst pos 0: " + linkedList.getFirst());
        linkedList.removeFirst();
    }

    @Override
    public void removeLast() {
        System.out.println("removeLast pos last: " + linkedList.getLast());
        linkedList.removeLast();
    }

    @Override
    public void getFirst() {
        System.out.println("getFirst: " + linkedList.getFirst());
        linkedList.getFirst();
    }

    @Override
    public void getLast() {
        System.out.println("getLast: " + linkedList.getLast());
        linkedList.getLast();
    }

    @Override
    public void title() {

    }

    @Override
    public void sortedType() {

    }

    @Override
    public void worstCase() {

    }
}
