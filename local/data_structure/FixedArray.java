package local.data_structure;

import local.impl.CommonImpl;

import java.util.Arrays;

public class FixedArray implements CommonImpl {

    @Override
    public void printContainer() {
        title(getClass().getSimpleName());
        System.out.println("\nFixed size, can be accessed using index String[]");
        String[] vehicleTypes = new String[3];
        vehicleTypes[0] = "pos1";
        vehicleTypes[1] = "pos2";
        System.out.println("Arrays" + Arrays.toString(vehicleTypes));
        System.out.println(vehicleTypes[0]);
        System.out.println(vehicleTypes[1]);
    }

    @Override
    public void title() {

    }

    @Override
    public void sortedType() {

    }

    @Override
    public void worstCase() {

    }
}
