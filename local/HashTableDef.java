package local;

import local.impl.CommonImpl;

import java.util.Hashtable;

public class HashTableDef implements CommonImpl {

    private final Hashtable<String, String> setHashTable;

    public HashTableDef(Hashtable<String, String> setHashTable) {
        this.setHashTable = setHashTable;
    }

    public void mainHashTable() {
        title(this.getClass().getSimpleName());
        System.out.println("**Symbol");;
        System.out.println("**Terms: recursive, log2n left, right, merge all partitions");
        System.out.println("**Technique: Divide and Conquer");
        sortedType(true, false);
        worstCase("O(n log n)");
    }

    public void getHashTable(String key) {
        setHashTable.get(key);

        System.out.println(key);
    }
    public void putHashTable() {
        setHashTable.put("Alice", "123-456-7890");
        setHashTable.put("Bob", "987-654-3210");
    }

    @Override
    public void printContainer() {
        title(this.getClass().getSimpleName());
        System.out.println("**Symbol");;
        System.out.println("**Terms: recursive, log2n left, right, merge all partitions");
        System.out.println("**Technique: Divide and Conquer");
        sortedType(true, false);
        worstCase("O(n log n)");
    }

    @Override
    public void title() {

    }

    @Override
    public void sortedType() {

    }

    @Override
    public void worstCase() {

    }
}
