package local.sorts;

import local.impl.CommonImpl;

public class RadixSort implements CommonImpl {

    @Override
    public void printContainer() {
        title(this.getClass().getSimpleName());
        sortedType(false, false);
        worstCase("O(nk), k is the # of digits in the largest array");
    }

    @Override
    public void title() {

    }

    @Override
    public void sortedType() {

    }

    @Override
    public void worstCase() {

    }
}
