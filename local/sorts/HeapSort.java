package local.sorts;

import local.impl.CommonImpl;

public class HeapSort implements CommonImpl {
    @Override
    public void printContainer() {
        title(this.getClass().getSimpleName());
        sortedType(false, false);
        worstCase("O(n log n)");
    }

    @Override
    public void title() {

    }

    @Override
    public void sortedType() {

    }

    @Override
    public void worstCase() {

    }
}
