package local.sorts;

import local.impl.CommonImpl;
import local.impl.QuickSortImpl;

public class QuickSortDef  implements QuickSortImpl, CommonImpl {

    @Override
    public void printContainer() {
        title(getClass().getSimpleName());
        System.out.println("**Symbol");;
        partition();
        swap();
        System.out.println("**Terms: Midpoint, pivot");
        System.out.println("**Technique: Divide and Conquer");
        sortedType(true, true);
        worstCase("O(n^2)");
    }

    @Override
    public void title() {

    }

    @Override
    public void partition() {
        System.out.println("Partition");
    }

    @Override
    public void swap() {
        System.out.println("Swap");
    }
    @Override
    public void sortedType() {}
    @Override
    public void worstCase() {}
}
