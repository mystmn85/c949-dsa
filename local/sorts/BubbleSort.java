package local.sorts;

import local.impl.CommonImpl;

public class BubbleSort implements CommonImpl
{
    @Override
    public void printContainer() {
        title(getClass().getSimpleName());
        sortedType(false, false);
        worstCase("O(n^2)");
        System.out.println("**Terms: swaps the results to the top");
    }

    @Override
    public void title() {

    }

    @Override
    public void sortedType() {

    }

    @Override
    public void worstCase() {

    }
}
