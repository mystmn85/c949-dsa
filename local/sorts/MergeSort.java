package local.sorts;

import local.impl.CommonImpl;
import local.impl.MergeSortImpl;

public class MergeSort implements MergeSortImpl, CommonImpl {

    public void printContainers() {
        title(getClass().getSimpleName());
        System.out.println("**Symbol");;
        partition();
        System.out.println("**Terms: recursive, log2n left, right, merge all partitions");
        System.out.println("**Technique: Divide and Conquer");
        sortedType(true, false);
        worstCase("O(n log n)");
    }

    @Override
    public void partition() {
        System.out.println("Partition");
    }

    @Override
    public void merge() {

    }

    @Override
    public void printContainer() {

    }

    @Override
    public void title() {

    }

    @Override
    public void sortedType() {

    }

    @Override
    public void worstCase() {

    }
}
