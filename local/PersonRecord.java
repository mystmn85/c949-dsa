package local;

public record PersonRecord(String name, int age) {

    // Record, previous area called fields
    public String toString() {
        return "Person{name='" + name + "', age=" + age + '}';
    }
}

