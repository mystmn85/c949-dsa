package local.impl;

public interface LinkedListImpl {
    void addFirst(Integer count);
    void removeFirst();
    void removeLast();
    void getFirst();
    void getLast();
}
