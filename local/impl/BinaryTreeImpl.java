package local.impl;

public interface BinaryTreeImpl {

    void hasTwoChildren();
    void nodeLeft();

    void nodeRight();
}
