package local.impl;

public interface MergeSortImpl {
    void partition();

    void merge();

}
