package local.impl;

public interface QuickSortImpl {
    void partition();
    void swap();
}
