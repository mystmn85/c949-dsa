package local.impl;

public interface CommonImpl {

    void printContainer();
    default void sortedType(boolean sort, boolean unsort) {
        String sorted = !sort ? "" : "sorted";
        String unsorted = !unsort ? "" : " and unsorted";
        System.out.println("**Sorted Type: " + sorted + unsorted);
    }
    default void worstCase(String complexity){
        System.out.println("**worstCase: " + complexity);
    };

    default void title(String className){
        System.out.printf("\n--- %s ---\n", className);
    }

    void title();

    void sortedType();

    void worstCase();
}
