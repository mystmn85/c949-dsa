package local;

import java.util.Stack;

public class CarsStacking {

    private final Stack<Integer> stacking;

    public CarsStacking(Stack<Integer> stacking) {
        this.stacking = stacking;
    }

    public Stack<Integer> colorsPush(Integer num) {
        stacking.push(num);
        return stacking;
    }

    public Stack<Integer> colorsPop() {
        stacking.pop();
        return stacking;
    }
}
