package local;

import local.data_structure.LinkedListDef;

import java.util.LinkedList;
import java.util.Stack;

public class MainDemo {

    public MainDemo() {}

    public static void main(String[] args) {
        CarsStacking cars = new CarsStacking(new Stack<Integer>());

        System.out.println("Push " + cars.colorsPush(6));
        System.out.println(cars.colorsPush(7));
        System.out.println("Pull " + cars.colorsPop());

        PersonRecord personRecord = new PersonRecord("Paul", 14);
        System.out.println(personRecord);

        // LinkedList w/ containers
        LinkedList<Integer> linkedList = new LinkedList<>();
        LinkedListDef linkedListDef = new LinkedListDef(linkedList);
        linkedListDef.printContainer();
//
//        // HashTable
//        HashTableDef hashTableDef = new HashTableDef(new Hashtable<>());
//        hashTableDef.mainHashTable();
//
//        //BinaryTree
//        BinaryTreeDef binaryTreeDef = new BinaryTreeDef();
//        binaryTreeDef.mainBinaryTree();
//
//        //QuickSort
//        QuickSortDef quickSortDef = new QuickSortDef();
//        quickSortDef.printContainer();
//
//        //MergeSort
//        MergeSort mergeSort = new MergeSort();
//        mergeSort.printContainers();
//
//        //SelectionSort
//        SelectionSort selectionSort = new SelectionSort();
//        selectionSort.printContainer();
//
//        //InsertionSort
//        InsertionSort insertionSort = new InsertionSort();
//        insertionSort.printContainer();
//
//        //BubbleSort
//        BubbleSort bubbleSort = new BubbleSort();
//        bubbleSort.printContainer();
//
//        //Array
//        FixedArray arrayDef = new FixedArray();
//        arrayDef.printContainer();
    }
}