package local;

import java.util.List;

public class BinaryTreeDef {

    private final static String FULL_TYPE = "Full";
    private final static String COMPLETE_TYPE = "COMPLETE";

    public BinaryTreeDef() {}

    public void mainBinaryTree() {
        System.out.println("\n--- BinaryTree Starts ---");
        leftNode();
        rightNode();
        binaryTypes();
    }
    public void leftNode() {
        System.out.println("Left node");
    }
    public void rightNode() {
        System.out.println("Right node");
    }

    public void binaryTypes() {
        List<String> types = List.of(FULL_TYPE, COMPLETE_TYPE);

        String exFull = "Full: Each node must contain 0 or 2 children";
        String exComplete = "Complete: Each node must be complete except in the last level, which are added at the left";

        System.out.printf("Types: %s:\n%s\n%s\n", types, exFull, exComplete);
    }

}
